Gestión de la cuenta
====================

UPMdrive utiliza las cuentas institucionales de la UPM. El usuario es la dirección de correo electrónico de la UPM y la contraseña la misma que se usa en el resto de los servicios.

Modificar la contraseña
***********************

Para cambiar la contraseña hay que acceder a la web https://www.upm.es/cuentasUPM/clave.upm

.. Para notas
.. topic:: Nota:

	Si se modifica la contraseña de la cuenta, el cambio afectará a todos los servicios de la UPM

.. image:: ../../html/images/img73.jpg
   :align: center
   :scale: 50 %

He olvidado la contraseña
*************************

En el caso de olvidar la contraseña hay que acceder a https://www.upm.es/gsr/correo_personal/clave.upm?olvido

.. Para notas
.. topic:: Nota:

	Si se modifica la contraseña de la cuenta, el cambio afectará a todos los servicios de la UPM

.. image:: ../../html/images/img74.jpg
   :align: center
   :scale: 50 %