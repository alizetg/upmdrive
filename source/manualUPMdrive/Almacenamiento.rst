==============
Almacenamiento
==============
Esta es una pequeña guía de instalación del cliente oficial de ownCloud para Windows y la configuración de la cuenta de UPMdrive en el mismo. Los pasos son similares en todas las plataformas con pequeños matices.

.. Para notas

.. topic:: Nota:

    Las direcciones https://drive.upm.es y https://nube.cesvima.upm.es/ son equivalentes. Se recomienda                                                                                         utilizar la dirección https://drive.upm.es para configurar el cliente.

Cliente de sincronización
-------------------------

El modo recomendado de acceder a UPMdrive es mediante el cliente de sincronización.

Instalación del cliente
"""""""""""""""""""""""

1. Descargar la última versión del cliente desde https://owncloud.org/install/#install-clients.

2. Arrancar el instalador.

.. para insertar una imagen

.. image:: ../../html/images/img1.jpg
    :scale: 100%

3. Configurar las opciones de instalación

Se puede elegir las opciones por defecto o personalizar los accesos. En todo caso se recomienda mantener la integración con Windows Explorer.

.. image:: ../../html/images/img2.jpg
    :scale: 100%

4. Elegir la ruta en la que se instalará el programa

.. image:: ../../html/images/img3.jpg
    :scale: 100%

5. Esperar a que finalice la instalación y pasar al siguiente paso.

.. image:: ../../html/images/img4.jpg
    :scale: 100%

6. Por último, se recomienda marcar la opción ejecutar ownCloud y terminar.

.. image:: ../../html/images/img5.jpg
    :scale: 100%

Configuración de UPMdrive
"""""""""""""""""""""""""

Al arrancar por primera vez se abrirá automáticamente el asistente de configuración.

1. Escribir la dirección del servidor UPMdrive (https://drive.upm.es)

.. topic:: Nota:

    Es muy importante utilizar el protolo seguro (HTTPs) para que la comunicación entre el cliente y el servidor sea cifrada y se protejan los datos.

.. image:: ../../html/images/img6.jpg
    :scale:  100%

2. Configurar los datos de la cuenta

Como nombre de usuario hay que indicar la dirección de correo de la UPM (opcionalmente, se puede prescindir de la parte @upm.es) y la contraseña será la misma que se utiliza para acceder al resto de servicios de la UPM.

.. image:: ../../html/images/img7.jpg
    :scale: 100%

En el caso de haber olvidado la contraseña puede restablecerla siguiendo las instrucciones en `Gestión de la cuenta <http://pagina2.readthedocs.io/en/latest/manualUPMdrive/4.Gestion%20de%20la%20cuenta.html>`_.

3. Seleccionar qué se quiere sincronizar y la carpeta local en la que se tendrán los ficheros sincronizados con la nube.


.. image:: ../../html/images/img8.jpg
    :scale: 100%

En la configuración por omisión, se sincroniza todo el contenido del servidor en la carpeta elegida. Es posible configurar que sólo se sincronice cierta parte.

4. Terminar la instalación

.. image:: ../../html/images/img9.jpg
    :scale: 100%

A partir de este momento todos los ficheros y cualquier cambio que se realice en la carpeta local elegida se subirán automáticamente al servidor.

Compartir entre usuarios
""""""""""""""""""""""""

Si en el paso 3 del proceso de Instalación del cliente  se ha activado la integración con Windows Explorer, se puede compartir fácilmente documentos entre usuarios sin necesidad de acceder a la web.

1. Abrir Windows Explorer en la carpeta local dónde se sincronizan los ficheros.

Una forma rápida de acceder es pulsar el botón derecho sobre el icono de *ownCloud* (una nube) que aparece en la bandeja del sistema (al lado del reloj) y elegir «Abrir la carpeta “…”».

2. Pulsar el botón derecho encima de la carpeta o archivo que se desea compartir.

.. image:: ../../html/images/img10.jpg
    :scale: 100%

3. Elegir la opción «Compartir con ownCloud».

4. Indicar el usuario con el que se desea compartir.

Se debe escribir la cuenta UPM de la persona con la    que se desea compartir y esperar unos segundos a que aparezca el nombre completo del usuario.

.. image:: ../../html/images/img11.jpg
    :scale: 100%

.. Para notas
.. topic:: Nota:

    El nombre de usuario es la parte izquierda del correo UPM (habitualmente, nombre.apellido) sin el @upm.es

5. Indicar los permisos

.. image:: ../../html/images/img12.jpg
    :scale: 100%

En el momento en que el otro usuario entre en la web, le aparecerá una nueva carpeta con los contenidos compartidos. Esa carpeta puede sincronizarla, moverla o renombrarla como si fuera propia.

Personalizar la sincronización
""""""""""""""""""""""""""""""

El cliente de *ownCloud* permite elegir qué carpetas se desean sincronizar en el equipo. A las carpetas que no se sincronicen se podrá acceder mediante la interfaz web o *webdav*. La selección se puede realizar al instalar el cliente o en cualquier momento posterior.

Durante la instalación
""""""""""""""""""""""

1. Cuando el asistente pregunta que se quiere sincronizar (paso 3 del proceso de configuración de UPMdrive) hay que seleccionar «Elegir qué sincronizar»
2. Elegir las carpetas que se quiere sincronizar y las que no.

.. image:: ../../html/images/img13.jpg
    :scale: 100%

Por ejemplo, si un usuario tuviera las carpetas *Documents* y *Photos* pero sólo quisiera sincronizar *Documents* ya que la carpeta *Photos* ocuparía mucho espacio en disco, basta con desmarcar la carpeta *Photos*.

3. Confirmar la selección pulsando «Ok»

4. El asistente mostrará las nuevas opciones en las que se verá como el espacio necesario para «sincronizar todo desde el servidor» y «elija qué sincronizar» difieren

.. image:: ../../html/images/img14.jpg
    :scale: 100%

5. Conectar la cuenta con las carpetas elegidas.

Modificar la sincronización
"""""""""""""""""""""""""""
Se puede modificar la sincronización en cualquier momento tantas veces como sea necesario para adaptarlo a las necesidades de cada dispositivo.

1.	Abrir el cliente
2.	Abrir el menú que se encuentra en la flecha a la izquierda del nombre de la carpeta («ownCloud» en este caso).

.. image:: ../../html/images/img15.jpg
    :scale: 100%

3.	Elegir las carpetas que se quieren sincronizar y las que no.

.. image:: ../../html/images/img16.jpg
     :scale: 100%

Para sincronizar una carpeta basta con marcar la casilla correspondiente o desmarcarlo para que deje de sincronizar. Por ejemplo, para empezar a sincronizar la carpeta *Photos* que no se eligió en el paso anterior basta con seleccionarla y darle a aplicar.

4.	Seleccionar «Aplicar» (sólo aparece cuando se haya realizado alguna modificación) para confirmar los cambios

La configuración de sincronización es específica de cada dispositivo: se puede tener un conjunto de carpetas sincronizadas en un ordenador y un conjunto diferente en otro.

Múltiples cuentas o usuarios
""""""""""""""""""""""""""""
El cliente de ownCloud permite sincronizar varias cuentas al mismo tiempo como puede ser, por ejemplo, el UPMdrive y un ownCloud delegado del grupo de investigación.

1.	Abrir el cliente

2.	Elegir una de las cuentas ya instaladas

3.	Seleccionar el botón «Cuenta» y, en el menú que aparece, «Añadir usuario»


.. image:: ../../html/images/img17.jpg
    :scale: 100%


4.	Se mostrará la ventana en la que se solicita la dirección del servidor para configurar una nueva cuenta tal y como se describe en Configuración de UPMdrive.
5.	Al finalizar el proceso, en la ventana del cliente aparecerán las cuentas que estén añadi-das

.. image:: ../../html/images/img18.jpg
    :scale: 100%

Desde el botón «Cuenta» del paso 3, se puede desconectar o eliminar una cuenta del cliente.

Interfaz web
------------
Otra forma de acceder a UPMdrive es mediante la interfaz web.

Este mecanismo es muy útil cuando se necesita acceder a los ficheros desde un PC o dispositivo que no tiene el cliente o que no poseemos.

Para acceder basta con acceder desde el navegador web a la dirección https://drive.upm.es/ utilizando como usuario el correo UPM (se puede prescindir de la parte @upm.es) y la contraseña del resto de servicios.

.. image:: ../../html/images/img19.jpg
    :scale: 80%

A través de la interfaz web se pueden subir y bajar archivos, mover archivos y carpetas dentro de UPMdrive y descargar carpetas.

.. image:: ../../html/images/img20.jpg
    :scale: 80%


.. Para notas
.. topic:: Nota:

    El tamaño máximo de archivo que se puede subir a través de la web es de 35 GB.
    No obstante, a través del cliente de sincronización (opción recomendada) o mediante Web-dav se pueden subir ficheros de mayor tamaño.


En la interfaz web también es posible acceder al área personal desde la que se pueden configurar algunas opciones

Compartir entre usuarios
""""""""""""""""""""""""

Es posible compartir archivos o toda una carpeta (con los archivos os subcarpetas que contenga) con otro usuario de UPMdrive.

.. Para notas
.. topic:: Nota:

	Hay que tener en cuenta que todo el espacio ocupado por una carpeta compartida se le imputa al usuario que la crea y comparte independientemente de qué usuario haya subido los ficheros.

Si se desea compartir la misma carpeta con más de un usuario sólo hay que repetir el proceso indicando en cada caso con qué usuario se desea compartir.

1.	Crear la nueva carpeta que se desea compartir

.. image:: ../../html/images/img21.jpg
     :scale: 80%


2.	Dar al botón de compartir

.. image:: ../../html/images/img22.jpg
     :scale: 80%

3.	Indicar el usuario con el que se desea compartir.

Se debe escribir la cuenta UPM de la persona con la que se desea compartir y esperar unos segundos a que aparezca el nombre completo del usuario.

.. image:: ../../html/images/img23.jpg
    :scale: 80%

.. Para notas
.. topic:: Nota:

	El nombre de usuario es la parte izquierda del correo UPM ( habitualmente, nombre.apellido) sin el @upm.es

4. Indicar los permisos

.. image:: ../../html/images/img24.jpg
     :scale: 80%

En el momento en que el otro usuario entre en la web, le aparecerá una nueva carpeta con los contenidos compartidos. Esa carpeta puede sincronizarla, moverla o renombrarla como si fuera propia


