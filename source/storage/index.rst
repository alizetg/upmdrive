**************
Almacenamiento
**************

El núcleo del servicio UPMdrive es el almacenamiento de ficheros: se almacena una copia de los documentos en el servidor que puede ser accedida mediante distintos mecanismos.

De los mecanismos existentes, se recomienda utilizar la sincronización: se almacena una copia en el servidor y otra en el disco local y una aplicación se encarga de actualizar las versiones coordinadas cuando se produzcan cambios. De esta forma se trabaja con los ficheros locales de la misma manera que como si no existiera UPMdrive, incluso aunque no haya conexión, pero al sincronizarlo

Sincronización
==============

El acceso sincronizado consiste en instalar una aplicación que compara los ficheros de cierta ubicación en el disco local con los del servidor y realiza los cambios necesarios para que ambas ubicaciones contengan los mismos ficheros.

Este es el modo recomendado al ser el más parecido a no tener UPMdrive: los ficheros realmente están en el disco local por lo que se accede a ellos de forma rápida incluso aunque no haya conexión. Y los cambios se actualizarán en el servidor (y en el resto de equipos) cuando ésta se recupere.

.. _owncloud_client_install:

Instalar cliente ownCloud
-------------------------

El primer paso es instalar el cliente que se encargue de la sincronización. En el caso de UPMdrive se utiliza el cliente de ownCloud (o cualquier otro compatible) sin ninguna personalización.

ownCloud proporciona clientes para `equipos de escritorio <https://owncloud.org/download/#owncloud-desktop-client>`_ y `aplicaciones para dispositivos móviles <https://owncloud.org/download/#owncloud-mobile-apps>`_.

..  note::

    Aunque se puede instalar cualquier versión, se recomienda utilizar la etiquetada como `production`.

1.  Ejecutar el instalador y seguir las instrucciones

    ..  image:: owncloud-client-install-windows-01.jpg
        :align: center

2.  Configurar las opciones de instalación según preferencias.

    ..  note:: Se recomienda mantener la integración con el administrador de ficheros.

    ..  image:: owncloud-client-install-windows-02.jpg
        :align: center

3.  Elegir la ruta en la que se instalará el programa

    ..  image:: owncloud-client-install-windows-03.jpg
        :align: center

4.  Esperar a que finalice la instalación y pasar al siguiente paso.

    ..  image:: owncloud-client-install-windows-04.jpg
        :align: center

5.  Por último, se recomienda marcar la opción ejecutar ownCloud y terminar.

    ..  image:: owncloud-client-install-windows-05.jpg
        :align: center

.. _setup_upm_drive:

Configurar UPMdrive
-------------------

El cliente de ownCloud permite :ref:`múltipes cuentas <owncloud_multi_account>` de servidores compatibles como se desee. La primera vez que se active el cliente tras la instalación se arrancará automáticamente el asistente de configuración para configurar la primera de ellas:

1.  Añadir un nuevo servidor.

    Indicar que el servidor es `https://drive.upm.es <https://drive.upm.es>`_.

    ..  image:: setup-upm-drive-windows-01.jpg
        :align: center

    ..  important::

        El acceso debe realizarse mediante HTTPs para que la comunicación entre cliente y servidor se realice cifrada.

2.  Proporcionar las credenciales de acceso a la cuenta.

    ..  image:: setup-upm-drive-windows-02.jpg
        :align: center

    Si se ha olvidado las credenciales de acceso se puede :ref:`restablecer la contraseña <upm_account_forget_passwd>`.

..  _owncloud_custom_sync_install:

3.  Seleccionar qué directorios locales y remotos deben sincronizarse.

    ..  image:: setup-upm-drive-windows-03.jpg
        :align: center

    En la configuración por omisión, se sincroniza todo lo almacenado en el servidor en una carpeta local. Si se selecciona «Elegir qué sincronizar» se pueden personalizar qué carpetas serán sincronizadas.

    Por ejemplo, si un usuario tuviera las carpetas *Documents* y *Photos* pero sólo quisiera sincronizar *Documents* en el equipo de trabajo, basta con desmarcar la carpeta *Photos*.

    ..  image:: setup-upm-drive-windows-04.jpg
        :align: center

    Tras confirmar la selección, el asistente cambiará la selacción e indicará el espacio necesario.

    ..  image:: setup-upm-drive-windows-05.jpg
        :align: center

    ..  note::

        El espacio necesario para «sincronizar todo desde el servidor» y «elija qué sincronizar» suele ser diferente cuando se han excluido carpetas.

A partir de este momento, todos los cambios que se realicen en las carpetas locales sincronizadas se replicarán en el servidor. Si se desea añadir otra cuenta, hay que repetir el proceso.

..  important::

    El cliente de ownCloud pregunta antes de sincronizar carpetas que ocupen más de 500 MB para evitar llenar el disco duro.

Configurar la sincronización
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

El cliente de *ownCloud* permite elegir qué carpetas se desean sincronizar en el equipo. A las carpetas que no se sincronicen se podrá acceder mediante la interfaz web o *webdav*. La selección se puede realizar al :ref:`instalar el cliente <owncloud_custom_sync_install>` y se puede cambiar en cualquier momento:

1.  Abrir el cliente

2.  Abrir el menú que se encuentra en la flecha a la izquierda del nombre de la carpeta («ownCloud» en este caso).

    ..  image:: setup-sync-windows-01.jpg
        :align: center

3.  Elegir las carpetas que se quieren sincronizar y las que no.

    ..  image:: setup-sync-windows-02.jpg
        :align: center

    Para sincronizar una carpeta basta con marcar la casilla correspondiente o desmarcarlo para que deje de sincronizar. Por ejemplo, para empezar a sincronizar la carpeta *Photos* que no se eligió durante la instalación basta con seleccionarla y darle a aplicar.

4.  Seleccionar «Aplicar» (sólo aparece cuando se haya realizado alguna modificación) para confirmar los cambios

La configuración de sincronización es específica de cada dispositivo: se puede tener un conjunto de carpetas sincronizadas en un ordenador y un conjunto diferente en otro.

.. _owncloud_multi_account:

Configurar múltiples cuentas
----------------------------

El cliente de ownCloud permite sincronizar varias cuentas al mismo tiempo del mismo o distintos servidores compatibles. Esto puede ser útil para, por ejemplo, sincronizar una cuenta personal y varias institucionales en UPMdrive y un ownCloud delegado del grupo de investigación.

1.  Abrir el cliente

2.  Elegir una de las cuentas ya instaladas

3.  Seleccionar el botón «Cuenta» y, en el menú que aparece, «Añadir usuario»

    ..  image:: multi-account-01.jpg
        :align: center

4.  Se abrirá el asistente de configuración de cuenta para crear una cuenta taly como se describe en :ref:`setup_upm_drive`.

    ..  note::

        Es necesario indicar el servidor que corresponda a cada cuenta.

5.  Al finalizar el proceso, en la ventana del cliente aparecerán las cuentas que estén añadidas

    ..  image:: multi-account-02.jpg
        :align: center

Desde el botón «Cuenta» del paso 3, se puede desconectar o eliminar una cuenta del cliente.

Interfaz web
============

La interfaz web está pensada para acceder puntualmente a ficheros desde un dispositivo que no tiene el cliente y que no se pueda o quiera instalarlo.

Basta con acceder a https://drive.upm.es/ con un navegador e iniciar sesión con el usuario adecuado (el correo pudiendo prescindirse de la parte @upm.es).

..  image:: web-01.jpg
    :align: center

A través de la interfaz web se pueden subir y bajar archivos, mover archivos y carpetas dentro de UPMdrive y descargar carpetas.

..  image:: web-02.jpg
    :align: center

..  important::

    El tamaño máximo de archivo que se puede subir a través de la web es de 35 GB.

    No obstante, a través del cliente de sincronización (opción recomendada) o mediante WebDAV se pueden subir ficheros de mayor tamaño.

En la interfaz web también es posible acceder al área personal desde la que se pueden configurar algunas opciones de la cuenta.

Compartir entre usuarios
========================
  
Es posible compartir archivos o toda una carpeta (con los archivos o subcarpetas que contenga) con otro usuario de UPMdrive.

..  important::

    Todo el espacio ocupado por una carpeta compartida se le imputa al usuario que la crea y comparte (el dueño) independientemente de qué usuario haya subido los ficheros.

Si se desea compartir la misma carpeta con más de un usuario sólo hay que repetir el proceso indicando en cada caso con qué usuario se desea compartir.

Una vez compartida una carpeta, a los destinatarios les aparecerá una carpeta con los datos compartidos. Esa carpeta puede sincronizarla, moverla o renombrarla como si fuera propia y sin que afecte al resto de usuarios con los que la comparten. Si la borra dejará de tener acceso a la carpeta pero la carpeta seguirá disponible para el dueño que se la compartió.

..  important::

    Si los contenidos de una carpeta compartida se borran o mueven fuera del recurso compartido desaparecerán para todos las personas que tengan dicha carpeta compartida incluído el dueño.

Gestor de archivos
------------------

Para compartir un fichero o carpeta con otro usuario de UPMdrive o mediante enlace con usuarios externos:

..  important::

    Es necesario haber activado la integración con el gestor de archivos durante la :ref:`instalación del cliente <owncloud_client_install>`.

1.  Abrir Windows Explorer en la carpeta local dónde se sincronizan los ficheros.

    ..  tip::

        Una forma rápida de acceder es pulsar el botón derecho sobre el icono de *ownCloud* (una nube) que aparece en la bandeja del sistema (al lado del reloj) y elegir «Abrir la carpeta “…”».

2.  Pulsar el botón derecho encima de la carpeta o archivo que se desea compartir.

    ..  image:: share-windows-01.jpg
        :align: center

3.  Elegir la opción «Compartir con ownCloud».

4.  Indicar el usuario con el que se desea compartir.

    Se debe escribir la cuenta UPM de la persona con la que se desea compartir y esperar unos segundos a que aparezca el nombre completo del usuario.

    ..  image:: share-windows-02.jpg
        :align: center

..  note::

    El nombre de usuario es la parte izquierda del correo UPM (habitualmente, nombre.apellido) sin el @upm.es

5.  Indicar los permisos

    ..  image:: share-windows-03.jpg
        :align: center

Interfaz web
------------

1.  Crear la nueva carpeta que se desea compartir

    ..  image:: share-web-01.jpg
        :align: center

2.  Dar al botón de compartir

    ..  image:: share-web-02.jpg
        :align: center

3.  Indicar el usuario con el que se desea compartir.

    ..  image:: share-web-03.jpg
        :align: center

    ..  note::

        Pueden pasar unos segundos hasta que se completa el nombre del usuario.

    ..  important::

        El nombre de usuario es la parte izquierda del correo UPM ( habitualmente, nombre.apellido) sin el @upm.es

4.  Indicar los permisos

    ..  image:: share-web-04.jpg
        :align: center
