.. UPMdrive documentation master file, created by
   sphinx-quickstart on Thu Feb  1 12:12:04 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Guia del usuario de UPMdrive
============================

..  centered:: ¡Bienvenido a UPMdrive!

A inicios de 2014 el `Centro de Supercomputación y Visualización de Madrid (CeSViMa) <//www.cesvima.upm.es>`_ lanza «Nube UPM»: un servicio de almacenamiento en la nube para los investigadores de la Universidad Politécnica de Madrid (UPM). Cuando CeSViMa pasa a depender del Vicerrectorado de Servicios Tecnológicos de la UPM en septiembre de 2016, este servicio se convierte en `UPMdrive <https://drive.upm.es>`_ que está disponible a todo el personal de la Universidad.

Basado en la tecnología `ownCloud <https://owncloud.org>`_, UPMdrive proporciona la capacidad de disponer de almacenamiento accesible desde cualquier lugar con conexión a Internet y desde cualquier dispositivo, ya sean ordenadores convencionales, tables, smartphones o cualquier otro dispositivo móvil con Android, iOS…

..  important::

    Esta documentación está en proceso de mejora contínua por lo que su contenido se modificará según se realicen actualizaciones.

    Los cambios importantes serán notificados a través de las listas de correo.

.. toctree::
    :caption: Contenidos
    :name: toc
    :maxdepth: 3
    :numbered:
    :glob:
    :includehidden:

    /storage/index
    /agenda/index
    /upm-account/index
    /cau

..  note::

    Para solucionar cualquier duda o problema que surja, el punto de contacto es el :ref:`Centro de Atención a Usuarios <cau>` del CeSViMa.
