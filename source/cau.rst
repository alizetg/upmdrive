.. _cau:

***********************
Obtener ayuda o soporte
***********************

Para solucionar cualquier duda o problema que surja, el punto de contacto es el `Centro de Atención a Usuarios <mailto:support@cesvima.upm.es>`_ del CeSViMa, dependiente del Vicerrectorado de Servicios Tecnológicos. El servicio se presta únicamente mediante correo electrónico.

En la consulta se debe de indicar siempre que sea posible y de la forma más clara:

* Descripción de la duda o problema surgido.

* Identificación del usuario (correo UPM).

Si se ha detectado algún problema, se debe añadir:

* Día, fecha y hora aproximados en la que se detectó la incidencia.

* Condiciones en las que se produce.

* Mensajes de error o información, si se dispone de ellos.

* Si la incidencia está relacionada con información almacenada, la ruta al directorio o fichero y, si está compartido, las cuentas que tienen acceso a él.

Cada una de las comunicaciones iniciadas queda automáticamente registrada en un sistema de control de incidencias asignándoles un identificador único y conservándose su histórico.
