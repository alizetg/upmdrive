.. _upm_account:

********************
Gestión de la cuenta
********************

UPMdrive utiliza las cuentas institucionales de la UPM. El usuario es la dirección de correo electrónico de la UPM (en el caso de UPMdrive se puede omitir el @upm.es) y la contraseña la misma que se usa en el resto de los servicios.

.. _upm_account_change_passwd:

Modificar la contraseña
=======================

Para cambiar la contraseña hay que acceder a la web https://www.upm.es/cuentasUPM/clave.upm

..  image:: change-passwd.jpg
    :align: center

..  important::

    Si se modifica la contraseña de la cuenta, el cambio afectará a todos los servicios de la UPM

.. _upm_account_forget_passwd:

He olvidado la contraseña
=========================

En el caso de olvidar la contraseña hay que acceder a https://www.upm.es/gsr/correo_personal/clave.upm?olvido

..  image:: forget-passwd.jpg
    :align: center

..  important::

    Si se modifica la contraseña de la cuenta, el cambio afectará a todos los servicios de la UPM
