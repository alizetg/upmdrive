***********************
Calendarios y contactos
***********************

Además del almacenamiento UPMdrive proporciona calendarios y agenda de contactos. Es posible acceder a estas funciones desde el menú superior (situado al lado del logo) de la interfaz web.

Tanto los calendarios como los contactos utilizan los protocolos estándar CalDAV y CardDAV, respectivamente, lo que permite que numerosas aplicaciones y dispositivos puedan acceder a ellos.

Configurar clientes
===================

Al utilizar los protocolos estándar CalDAV y CardDAV es posible acceder a los calendarios y contactos utilizando múltiples clientes. En algunos casos, la integración es nativa, mientras que en otros es necesario instalar y configurar alguna pequeña extensión, plugin o aplicación que haga de traductor entre el cliente y el servidor.

Aunque se describe cómo se configuran algunos de los más utilizados, los pasos deberían ser equivalentes para cualquier otro cliente.

..  toctree::
    :glob:

    client/*

Compartir calendarios o contactos
=================================

Al igual que con los ficheros, es posible compartir calendarios o contactos entre usuarios de UPMdrive. Desde la aplicación adecuada (seleccionada en el menú superior al lado del logo).

1.  Pulsar el símbolo de compartir situado al lado de cada calendario

2.  Indicar el usuario con el que se desea compartir.

    ..  image:: share-web-01.jpg
        :align: center

    ..  note::

        Pueden pasar unos segundos hasta que se completa el nombre del usuario.

    ..  important::

        El nombre de usuario es la parte izquierda del correo UPM ( habitualmente, nombre.apellido) sin el @upm.es

3.  Indicar los permisos

    ..  image:: share-web-02.jpg
        :align: center
