*******************
Mozilla Thunderbird
*******************

Mozilla Thunderbird dispone de dos extensiones para poder acceder a los calendarios (*Ligthing*) y contactos (*Sogo connector*).

Calendarios
***********

Mozilla Thunberdird tiene una extensión denominada Lighting capaz de sincronizar los calenarios. Esta extensión sólo permite añadir un calendario individual por lo que los pasos hay que repetirlos para cada calendario que se desee añadir.

1.  Abrir el cliente de calendarios en Mozilla Thunderbird

    ..  image:: thunderbird-01.jpg
        :align: center

2.  Abrir el menú contextual y seleccionar nuevo calendario

    ..  image:: thunderbird-02.jpg
        :align: center

3.  Seleccionar un calendario CalDAV

    ..  image:: thunderbird-03.jpg
        :align: center

4.  Indicar la dirección del calendario

    Esta dirección se debe obtener desde la interfaz web y tendrá el formato: https://drive.upm.es/remote.php/dav/calendars/[nombre.apellido]/[calendario]/

    ..  image:: thunderbird-04.jpg
        :align: center
