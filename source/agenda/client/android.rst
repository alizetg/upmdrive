********************
Dispositivos Android
********************

Para poder acceder al calendario y los contactos desde un dispositivo Android, es necesario instalar algún cliente que proporcione el soporte de los protocolos.

Aunque existen numerosas opciones, se van a describir dos. Cualquier otra alternativa debe funcionar de forma similar:

DavDroid
========

1.  Instalar la aplicación Dav Droid desde la Play Store.

    ..  image:: android-davdroid-01.jpg
        :scale: 30%

    ..  image:: android-davdroid-02.jpg
        :scale: 30%

    ..  image:: android-davdroid-03.jpg
        :scale: 30%

    ..  image:: android-davdroid-04.jpg
        :scale: 30%

    ..  image:: android-davdroid-05.jpg
        :scale: 30%

    ..  image:: android-davdroid-06.jpg
        :scale: 30%

2.  Abrir la aplicación

    La primera vez que se abre aparecerán dos diálogos: uno indicando la licencia y otro indicando una alternativa para sincronizar tareas (se recomienda seleccionar «*Don’t show again*»).

    ..  image:: android-davdroid-07.jpg
        :scale: 30%

    ..  image:: android-davdroid-08.jpg
        :scale: 30%

3.  Añadir una nueva cuenta, pulsando en el botón + situado abajo a la derecha.

4.  Elegir «Login with URL and username».

    ..  image:: android-davdroid-09.jpg
        :scale: 30%

    ..  image:: android-davdroid-10.jpg
        :scale: 30%

5.  Indicar como dirección https://drive.upm.es

6.  Indicar el usuario (correo UPM, siendo opcional la parte @upm.es) y contraseña (la misma utilizada en el resto de servicios.

    ..  image:: android-davdroid-11.jpg
        :scale: 30%

7.  Indicar en la descripción el nombre que se quiere dar a la cuenta (recomendado: UPMdrive).

8.  Elegir el nombre de la cuenta y confirmar seleccionando «Create Account».

    ..  image:: android-davdroid-12.jpg
        :scale: 30%

9.  En la pantalla de inicio, aparecerá la cuenta creada.

    ..  image:: android-davdroid-13.jpg
        :scale: 30%

10. Seleccionar la cuenta.

11. Elegir los calendarios y contactos que se quieren sincronizar.

12. Forzar una sincronización seleccionando el icono de refrescar en la parte superior derecha.

    ..  image:: android-davdroid-14.jpg
        :scale: 30%

    ..  image:: android-davdroid-15.jpg
        :scale: 30%

A partir de este momento, entre los calendarios y contactos aparecerán los de UPMdrive

    ..  image:: android-davdroid-16.jpg
        :align: center

F-Droid
-------

DavDroid es una aplicación de pago. No obstante, existe un repositorio online de apps donde se puede encontrar de forma gratuita.

Dicho repositorio es F-Droid, y se puede instalar Dav Droid con él de la siguiente forma:

1.  Desde el navegador web, buscar F-Droid y acceder a la página oficial.

    ..  image:: android-fdroid-01.jpg
        :scale: 30%

    ..  image:: android-fdroid-02.jpg
        :scale: 30%

2.  Pulsar en el enlace para descargar. Cuando salga el cuadro de diálogo, pulsar en Abrir la carpeta, con lo que podremos visualizar la app en el directorio en el que se ha guardado:

    ..  image:: android-fdroid-03.jpg
        :scale: 30%

    ..  image:: android-fdroid-04.jpg
        :scale: 30%

3.  Seleccionar el archivo para instalar. Confirmar dicho proceso.

    ..  image:: android-fdroid-05.jpg
        :scale: 30%

    ..  image:: android-fdroid-06.jpg
        :scale: 30%

4.  Abrir F-Droid. Pulsar en el botón de búsqueda y escribir Dav Droid, con lo que saldrá dicha aplicación disponible para su descarga.

    ..  image:: android-fdroid-07.jpg
        :scale: 30%

    ..  image:: android-fdroid-08.jpg
        :scale: 30%

    ..  image:: android-fdroid-09.jpg
        :scale: 30%

5.  Pulsar el botón de instalar. Confirmando en la siguiente pantalla pulsando de nuevo en instalar obtendremos Dav Droid de forma gratuita.

    ..  image:: android-fdroid-10.jpg
        :scale: 30%

    ..  image:: android-fdroid-11.jpg
        :scale: 30%

Open Sync
=========

Otra alternativa es Open Sync, la cual funciona de manera similar y se puede instalar de la siguiente forma:

1.  Instalar la aplicación Open Sync desde la Play Store.

    ..  image:: android-opensync-01.jpg
        :scale: 33%

2.  Abrir la aplicación

    La primera vez que se abre aparecerán dos diálogos: uno indicando la licencia y otro indi-cando una alternativa para sincronizar tareas (se recomienda seleccionar «*Don’t show again*»).

    ..  image:: android-opensync-02.jpg
        :scale: 33%

    ..  image:: android-opensync-03.jpg
        :scale: 33%

3.  Añadir una nueva cuenta, pulsando en el botón + situado abajo a la derecha.

4.  Elegir «Login with URL and username».

5.  Indicar como dirección https://drive.upm.es

6.  Indicar el usuario (correo UPM, siendo opcional la parte @upm.es) y contraseña (la misma utilizada en el resto de servicios).

7.  Indicar en la descripción el nombre que se quiere dar a la cuenta (recomendado: UPMdrive)

8.  Confirmar los datos seleccionando «Login»

9.  Elegir el nombre de la cuenta y confirmar seleccionando «Create Account»

    ..  image:: android-opensync-04.jpg
        :align: center


10. En la pantalla de inicio, aparecerá la cuenta creada.

11. Seleccionar la cuenta

12. Elegir los calendarios y contactos se quieren sincronizar.

13. Forzar una sincronización seleccionando el icono de refrescar en la parte superior derecha.

    A partir de este momento, en el calendario y contactos aparecerán los de UPMdrive

    ..  image:: android-opensync-05.jpg
        :align: center
