*****************
Microsoft Outlook
*****************

Para poder utilizar los calendarios y contactos de UPMdrive en Microsoft Outlook es necesario instalar y configurar una extensión

1.  Instalar el plugin `CalDav Synchronizer <http://caldavsyncronizer.org/>`_  con Microsoft Outlook cerrado.

2.  Abrir Microsoft Outlook

3.  Ir a calendario

4.  Abrir la nueva pestaña CalDAV Synchronizer

    ..  image:: outlook-01.jpg
        :align: center

5.  Abrir Synchronization Profiles

6.  Seleccionar «Add a new profile»

7.  Elegir NextCloud

    ..  image:: outlook-02.jpg
        :align: center

8.  Rellenar los datos de conexión a UPMdrive:

    ..  image:: outlook-03.jpg
        :align: center

    ..  image:: outlook-04.jpg
        :align: center

9.  Seleccionar «Test or Discover settings».

10. En el listado de calendarios disponible en UPMdrive elegir uno de ellos

    ..  image:: outlook-05.jpg
        :align: center


11. Elegir la dirección e intervalo de sincronización


12. Confirmar con «Synchronize Now»
