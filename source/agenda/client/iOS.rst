****************
Dispositivos iOS
****************

iOS soporta nativamente los protocolos CalDAV (calendarios) como CardDAV (contactos).

Calendario
==========

1.  Abrir la aplicación de ajustes

2.  Seleccionar «Calendario»

3.  Seleccionar «Cuentas»

4.  Seleccionar «Añadir cuenta»

5.  Seleccionar «Otra en el tiempo de cuenta»

6.  Seleccionar «Añadir cuenta CalDAV»

    ..  image:: ios-calendar-01.jpg
        :align: center

7.  Rellenar el campo servidor con https://drive.upm.es/remote.php/dav/principals/users/[USERNAME]/

    .. image:: ios-calendar-02.jpg
        :align: center
        :height: 400px

    ..  note::

        La URL se puede obtener desde la parte inferior de la interfaz web.

        ..  image:: ../webdav-link-calendar.jpg
            :align: center

8.  Indicar el usuario (correo UPM, siendo opcional la parte @upm.es) y contraseña (la misma utilizada en el resto de servicios).

9.  Indicar en la descripción el nombre que se quiere dar a la cuenta (recomendado: UPMdrive)

10. Seleccionar «Siguiente»

El dispositivo deberá validar la configuración y mostrar los calendarios en la aplicación Calendario.

Contactos
=========

1.  Abrir la aplicación de ajustes

2.  Seleccionar «Contactos»

3.  Seleccionar «Cuentas»

4.  Seleccionar «Añadir cuenta»

5.  Seleccionar «Otra en el tiempo de cuenta»

6.  Seleccionar «Añadir cuenta CardDAV»

    ..  image:: ios-contacts-01.jpg
        :align: center

7.  Rellenar el campo servidor con https://drive.upm.es/remote.php/dav/principals/users/[USERNAME]/

    .. image:: ios-contacts-02.jpg
        :align: center
        :height: 400px

    ..  note::

        La URL se puede obtener desde la parte inferior de la interfaz web.

        ..  image:: ../webdav-link-contacts.jpg
            :align: center

8.  Indicar el usuario (correo UPM, siendo opcional la parte @upm.es) y contraseña (la misma utilizada en el resto de servicios).

9.  Indicar en la descripción el nombre que se quiere dar a la cuenta (recomendado: UPMdrive)

10. Seleccionar «Siguiente»

El terminal deberá validar la configuración y mostrar los calendarios en la aplicación Contactos.
